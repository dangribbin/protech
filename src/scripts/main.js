function closeMobileNav () {
  $('nav.mobile').hide();
  $('body, html').removeClass('noscroll');
}

function openMobileNav () {
  $('body, html').addClass('noscroll');
  $('nav.mobile').show();
}


$('.mobile-nav-close-button').on('click', closeMobileNav)
$('.mobile-nav-open-button').on('click', openMobileNav)

$('.model-button').on('click', function () {
  var clickedModelName = $(this).data('model-name');
  var clickedModelImageSource = $(this).data('model-image-src');
  $(this).parent().find('.active').removeClass('active');
  $('.model-image').attr('src', clickedModelImageSource);
  $(this).addClass('active');
  $('.table-outer').hide();
  $('.' + clickedModelName + '-table').show();
});

$('.loader-table').show();


$('.feature-button').on('click', function () {
  var newTitleText = $(this).data('feature-title');
  var newDescriptionText = $(this).data('feature-description');
  var newIconSrc = $(this).data('feature-icon');

  $('.feature-title-text').text(newTitleText);
  $('.feature-description-text').text(newDescriptionText);
  $('.feature-icon').attr('src', newIconSrc);
});